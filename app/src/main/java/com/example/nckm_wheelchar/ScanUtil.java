package com.example.nckm_wheelchar;

import android.app.Activity;
import com.google.zxing.integration.android.IntentIntegrator;

public class ScanUtil {

    private ScanUtil(){}

    public static void Scan(Activity activity){
        IntentIntegrator integrator =new IntentIntegrator(activity);
        integrator.setOrientationLocked(false);
        integrator.setCaptureActivity(CaptureAnyOrientationActivity.class);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("請掃描");
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

}
