package com.example.nckm_wheelchar;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import cz.msebera.android.httpclient.Header;

public class Download {

    public Activity activity;
    public String version;      //版本
    public Tools tools;         //常用工具
    private String downUrl;     //下載路徑

    public Download(Activity mA) {
        this.activity = mA;
        this.tools = new Tools(mA);
    }

    /**
     * 開始執行更新程序
     * @param XML_App_Name 線上XML APP NAME
     */
    public void run(String XML_App_Name){
        try {
            version = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (tools.isNetwork()){ //判斷是否有網路
            final String service = "GetInfo?APP="+XML_App_Name;
            final String loginURL = activity.getResources().getString(R.string.url_ckv)+service;
            AsyncHttpClient client = new AsyncHttpClient();
            client.get(loginURL, null, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        String htmltext =  new String(responseBody,"UTF-8");
                        JSONObject response = new JSONObject(htmltext);
                        JSONObject AppInfo = response.getJSONObject("GetInfoResult");
                        String updateMsg = AppInfo.optString("updateMsg");
                        downUrl = AppInfo.optString("url");
                        String vCode = AppInfo.optString("versionCode");
                        if(Float.parseFloat(version) < Float.parseFloat(vCode)){
                            showDownloadDialog(updateMsg);
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    tools.showToast("檢測版本失敗",2,3000);
                }
            });

        }else{
            tools.showAlertDialog("網路","目前沒有開啟網路，請確保網路是保持連線的狀態。");
        }
    }

    /**
     * 詢問是否更新提示窗
     * @param updateMsg 新版APK 修改訊息
     */
    private void showDownloadDialog(String updateMsg){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("軟體版本更新");
        builder.setMessage(updateMsg);
        builder.setPositiveButton("下載", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Thread downLoadThread = new Thread(DownApkRunnable);
                downLoadThread.start();
            }
        });
        builder.setNegativeButton("以後再說", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Dialog noticeDialog = builder.create();
        noticeDialog.show();
    }

    //region 下載APK
    public Runnable DownApkRunnable = new Runnable() {
        @Override
        public void run() {
            //檔案名稱
            String filename = downUrl.substring(downUrl.lastIndexOf("/") + 1);
            //取得文件名稱
            URL myURL = null;
            try {
                myURL = new URL(downUrl);
                URLConnection conn = myURL.openConnection();
                conn.connect();
                InputStream is = conn.getInputStream();
                int fileSize = conn.getContentLength();//根據響應獲取文件大小
                if (fileSize <= 0) throw new RuntimeException("無法獲取檔案 ");
                if (is == null) throw new RuntimeException("stream is null");
                File ApkFile = new File("/sdcard/"+filename);
                FileOutputStream fos = new FileOutputStream(ApkFile);
                byte buf[] = new byte[1024];
                do{
                    int numread = is.read(buf);
                    if(numread <= 0){
                        break;
                    }
                    fos.write(buf,0,numread);
                }while(true);

                AppInstall(Uri.parse("file://" + ApkFile.toString()));

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };
    //endregion

    /**
     * 開啟APK 更新
     * @param url 本地APK路徑
     */
    private void AppInstall(Uri url) {
        Intent promptInstall = new Intent(Intent.ACTION_VIEW)
                .setDataAndType(url, "application/vnd.android.package-archive");
        promptInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(promptInstall);
    }

}
