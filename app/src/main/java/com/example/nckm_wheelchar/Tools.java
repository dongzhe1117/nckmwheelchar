package com.example.nckm_wheelchar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.support.v7.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Tools {

    Context context;
    boolean isRun;
    private ProgressDialog pd;
    Thread thread;
    public Tools( Context context){
        this.context = context;
    }

    /**
     *  取得WCF 連線位置
     * @return URL
     */
    public String GetWcfUrl(){
        return context.getResources().getString(R.string.url);
    }

    /**
     *  連線失敗時提示相關可能原因
     */
//    public void showConnFailure(){
//
//        showAlertDialog(context.getResources().getString(R.string.Error_Network_Tiile)
//                ,context.getResources().getString(R.string.ConnFailure));
//
//    }

    /**
     *  Toast 提示視窗
     *  <br>1.center 水平垂直皆置中</br>
     *  <br>2.center_vertical 垂直置中</br>
     *  <br>3.center_horizontal 水平置中</br>
     *  <br>4.top 置頂</br>
     *  <br>5.left 置左</br>
     *  <br>6.bottom 置底</br>
     *  <br>7.right 置右</br>
     * @param msg 內容
     * @param gravity   顯示位置
     *  @param duration 顯示時間
     */
    public void showToast(String msg, int gravity,int duration ){

        final Toast toast = Toast.makeText(context,msg,Toast.LENGTH_LONG);
        switch (gravity){
            case 1:
                toast.setGravity(Gravity.CENTER,0,0);
                break;
            case 2:
                toast.setGravity(Gravity.CENTER_VERTICAL ,0,0);
                break;
            case 3:
                toast.setGravity(Gravity.CENTER_HORIZONTAL ,0,0);
                break;
            case 4:
                toast.setGravity(Gravity.TOP ,0,0);
                break;
            case 5:
                toast.setGravity(Gravity.LEFT ,0,0);
                break;
            case 6:
                toast.setGravity(Gravity.BOTTOM ,0,0);
                break;
            case 7:
                toast.setGravity(Gravity.RIGHT ,0,0);
                break;
        }


        View toastView = toast.getView();
        TextView text = (TextView) toastView.findViewById(android.R.id.message);
        text.setTextColor(Color.BLACK);
        toastView.setBackgroundResource(R.drawable.bg_toast_w);

        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(duration, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                toast.show();
            }
            public void onFinish() {
                toast.cancel();
            }
        };

        toast.show();
        toastCountDown.start();

    }

    /**
     * 關閉鍵盤
     * @param context Application Context
     * @param myEt EditText
     */
    public void closeKeyboard(Context context, EditText myEt){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//        隱藏鍵盤
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//        關閉鍵盤
        imm.hideSoftInputFromWindow(myEt.getWindowToken(), 0);
    }

    /**
     * 檢查是否有網路狀態
     * @return 是/否
     */
    public boolean isNetwork(){
        ConnectivityManager CM = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = CM.getActiveNetworkInfo();
        if (info == null || !info.isAvailable())  //判斷是否有網路
            return false;
        else
            return true;
    }

    /**
     * @param Title 標題
     * @param msg 內容
     * @param isRun 開關
     */
    public void showWaitDialog(String Title,String msg,Boolean isRun){
        this.isRun = isRun;
        pd = ProgressDialog.show(context, Title, msg,true);
        thread  = new Thread(runnable);
        thread.start();
    }
    Runnable runnable = new Runnable(){
        @Override
        public void run() {
            try {
                do{
                    Thread.sleep(1000);
                }while (isRun);
                pd.dismiss();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 跳出提示視窗
     * @param context　Application Context
     */
    public void ShowNetworkDialog(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);   // 這是用來設定點選 Dialog 的外部可不可以關閉這個 Dialog
        builder.setTitle("網路狀態");
        builder.setMessage("請先開啟網路，再重新操作一次");
        builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     *  關閉 Progress Dialog 提示窗
     */
    public void setIsRunFalse(){
        this.isRun = false;
        pd.dismiss();
    }

    /**
     * 跳出提示視窗
     * @param Title 標題
     * @param msg 內容
     */
    public void showAlertDialog(String Title,String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);   // 這是用來設定點選 Dialog 的外部可不可以關閉這個 Dialog
        if (!Title.equals(""))
            builder.setTitle(Title);
        builder.setMessage(msg);
        builder.setPositiveButton("知道了", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * @return URL
     */
    public String GetUrl(){
        return context.getResources().getString(R.string.url);
    }

//    public String GetGP(){
//        return context.getResources().getString(R.string.gp);
//    }

    /**
     *  長度不足2 前面補0
     * @param data 輸入値
     * @return 輸出値
     */
    public String formatAns(int data){
        return String.format( "%02d", data);
    }

}
